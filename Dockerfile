FROM ubuntu:20.10

RUN apt update -y && apt upgrade -y && apt install -y curl
RUN adduser --disabled-password --gecos "" --home /home/container container

USER container

ENV USER=container HOME=/home/container
WORKDIR /home/container
COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
